# Lab 2 -- Linter and SonarQube as a part of quality gates

[![pipeline status](https://gitlab.com/sqrlabs/s23-lab2-linter-and-sonar-as-a-part-of-qg/badges/master/pipeline.svg)](https://gitlab.com/sqrlabs/s23-lab2-linter-and-sonar-as-a-part-of-qg/-/commits/master)

Sonarqube report:
![img.png](screenshots/img.png)
